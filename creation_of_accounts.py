from pyvin import VIN
import datetime
import numpy as np
from pandas.io import gbq
import pandas as pd
from google.cloud import bigquery
from dateutil.relativedelta import relativedelta
from uszipcode import SearchEngine

def create_accounts():
    client = bigquery.Client(project='datalakewarehouse')

    pd.set_option('display.max_columns', None)

    q = '''SELECT * FROM `datalakewarehouse.d1_datasets.Silvervine_Policy`'''
    policy = gbq.read_gbq(q)

    q = '''SELECT insuredID ,email,mailingAddressID,phone2,eDelivery,addDate from datalakewarehouse.d1_datasets.Silvervine_Insured'''
    insured = gbq.read_gbq(q)

    insured = insured.sort_values('addDate', ascending=False)
    insured = insured.drop_duplicates(subset='insuredID', keep='first')
    insured = insured.rename(columns=({'addDate': 'insuredAddDate'}))
    policy = policy.merge(insured, how='left', on='insuredID')
    policy.email = policy.email.str.lower()
    policy.effectiveDate = pd.to_datetime(policy.effectiveDate)
    policy.expirationDate = pd.to_datetime(policy.expirationDate)
    policy.applicationDate = pd.to_datetime(policy.applicationDate)
    policy.insuredAddDate = pd.to_datetime(policy.insuredAddDate)
    policy.expirationDate = pd.to_datetime(policy.expirationDate)

    # grab additional driver, vehicle, address information
    q = '''SELECT paymentPlanID, planName from  datalakewarehouse.d1_datasets.Silvervine_PaymentPlan'''
    paymentplan = gbq.read_gbq(q)

    q = '''SELECT * FROM `datalakewarehouse.d1_datasets.Silvervine_Driver`'''
    driver = gbq.read_gbq(q)

    q = '''SELECT * FROM `datalakewarehouse.d1_datasets.Silvervine_Vehicle`'''
    vehicles = gbq.read_gbq(q)

    q = '''SELECT * FROM `datalakewarehouse.d1_datasets.Silvervine_Auto`'''
    auto = gbq.read_gbq(q)

    q = '''SELECT * FROM `datalakewarehouse.d1_datasets.Silvervine_DriverAccident`'''
    driver_accident = gbq.read_gbq(q)

    q = '''SELECT * FROM `datalakewarehouse.d1_datasets.Silvervine_Coverages`'''
    coverages = gbq.read_gbq(q)

    q = '''SELECT * FROM `datalakewarehouse.d1_datasets.Silvervine_Address`'''
    address = gbq.read_gbq(q)
    address = address.rename(columns={'id': 'mailingAddressID'})

    q = '''SELECT * FROM `datalakewarehouse.d1_datasets.Silvervine_Insured`'''
    insured = gbq.read_gbq(q)

    policy = policy.merge(paymentplan, how='left', on='paymentPlanID')

    # fill white space with nulls for all tables
    policy = policy.replace(r'^\s*$', np.nan, regex=True)
    insured = insured.replace(r'^\s*$', np.nan, regex=True)
    paymentplan = paymentplan.replace(r'^\s*$', np.nan, regex=True)
    driver = driver.replace(r'^\s*$', np.nan, regex=True)
    vehicles = vehicles.replace(r'^\s*$', np.nan, regex=True)
    auto = auto.replace(r'^\s*$', np.nan, regex=True)
    driver_accident = driver_accident.replace(r'^\s*$', np.nan, regex=True)
    coverages = coverages.replace(r'^\s*$', np.nan, regex=True)
    address = address.replace(r'^\s*$', np.nan, regex=True)

    # Rate call
    # 98 - Submission
    # RC1A - 99 no premium total
    # RC1B - 99 with premium total
    # RC2 - 0, remove renewals
    # 4 - Declined
    # 6 - Not_Written
    # 1 - Active
    # 2 - Bound
    # 3 - Canceled
    # 5  - Expired


    # hierarchy of importance
    # Active -> Bound -> Canceled - > Expired -> Not_Written -> RC2 -> RC1B -> RC1A

    policy = policy.sort_values(['email', 'insuredAddDate'], ascending=[True, True])

    policy.loc[(policy.status == 99) & (policy.premiumTotal == 0), 'nstatus'] = 'RC1A'
    policy.loc[(policy.status == 99) & (policy.premiumTotal > 0), 'nstatus'] = 'RC1B'
    policy.loc[(policy.status == 0), 'nstatus'] = 'RC2'
    policy.loc[(policy.status == 1), 'nstatus'] = 'Active'
    policy.loc[(policy.status == 3), 'nstatus'] = 'Canceled'
    policy.loc[(policy.status == 5), 'nstatus'] = 'Expired'
    policy.loc[(policy.status == 6), 'nstatus'] = 'Not_Written'
    policy.loc[(policy.status == 4), 'nstatus'] = 'Declined'
    policy.loc[(policy.status == 2), 'nstatus'] = 'Bound'
    policy.loc[(policy.status == 98), 'nstatus'] = 'Submission'  # Not used by silvervine
    policy.loc[(policy.status == 97), 'nstatus'] = 'Claims_Only'
    policy = policy.drop(columns=['status'])
    policy = policy.rename(columns=({'nstatus': 'status'}))

    # remove any policies with null premium totals
    policy = policy[~policy.premiumTotal.isna()]

    # move canceled policies to expired once expiration date is exceeded
    today = datetime.date.today()
    policy.loc[((policy.status == 'Canceled') & (policy.expirationDate.dt.date < today)), 'status'] = 'Expired'

    # keep tally of previous policies amount paid and tally it onto the amount paid column, so the lifetime earned written premium always accumulates.
    expired_amount_paid = policy[policy.status == 'Expired'].groupby('email').agg(
        histAmountPaid=('amountPaid', 'sum')).reset_index()

    # keep tally of last expired policy date
    last_expiration = policy[policy.status == 'Expired'].sort_values('expirationDate').groupby('email').agg(
        last_loop_exp_date=('expirationDate', 'last')).reset_index()
    last_expiration.last_loop_exp_date = pd.to_datetime(last_expiration.last_loop_exp_date)

    # keep tally of first/last touch point (newest date first, so first touch is the last, last touch is first)
    first_touch_point = policy[policy.isRenewal == 0].groupby('email').agg(
        fTouchDate=('insuredAddDate', 'first')).reset_index()
    last_touch_point = policy[policy.isRenewal == 0].groupby('email').agg(
        lTouchDate=('insuredAddDate', 'last')).reset_index()

    # keep tally of number of quotes generated by customer, make sure renewals dont count as a generated quote by user.
    policy['total_quotes'] = 0
    for status in policy.status.unique():
        temp = policy[(policy.status == status) & (policy.isRenewal == 0)].groupby('email').agg(
            status=('policyID', 'count')).reset_index()
        temp = temp.rename(columns={'status': f'num_{status}_quotes'})
        policy = policy.merge(temp, how='left', on='email')
        policy[f'num_{status}_quotes'] = policy[f'num_{status}_quotes'].fillna(0)
        policy['total_quotes'] = policy['total_quotes'] + policy[f'num_{status}_quotes']

    # merge previous policy amountPaid sum to policy
    policy = policy.merge(expired_amount_paid, how='left')
    policy = policy.merge(last_expiration, how='left')
    policy = policy.merge(first_touch_point, how='left')
    policy = policy.merge(last_touch_point, how='left')

    # fill rest of polcies amount paid with 0
    policy.histAmountPaid = policy.histAmountPaid.fillna(0)

    # ----------------------------------------------------------------------CREATE ACCOUNTS TABLE ---------------------------------------------------------------------------------------
    # make sense of the gender and marital status columns in drivers table
    driver.loc[driver.gender == 'M', 'gender'] = 'Male'
    driver.loc[driver.gender == 'F', 'gender'] = 'Female'
    driver.loc[driver.gender == 'U', 'gender'] = 'Male'
    driver.loc[driver.gender == 'X', 'gender'] = 'Non-Binary'
    driver.loc[driver.gender.isna(), 'gender'] = 'Unknown'

    driver.loc[driver.maritalStatus == 10, 'nmaritalStatus'] = 'Single'
    driver.loc[driver.maritalStatus == 0, 'nmaritalStatus'] = 'Married'
    driver.loc[driver.maritalStatus.isna(), 'nmaritalStatus'] = 'Unknown'
    driver = driver.drop(columns=['maritalStatus'])
    driver = driver.rename(columns=({'nmaritalStatus': 'maritalStatus'}))

    auto_s = auto[['policyID', 'priorCarrier', 'priorYearsWithCompany', 'priorMonthsWithCompany', 'priorPolicyExpDate']]
    address_s = address[['mailingAddressID', 'street', 'street2', 'city', 'state', 'zipcode', 'county']]
    # merge policy to other tables needed
    policy = policy.merge(address_s, how='left', on='mailingAddressID')
    policy = policy.merge(auto_s, how='left', on='policyID')

    # vehicle / driver level

    # get driver age from todays date and their birthday
    driver.dob = pd.to_datetime(driver.dob)


    def from_dob_to_age(born):
        today = datetime.date.today()
        age = today.year - born.year - ((today.month, today.day) < (born.month, born.day))
        return age


    driver['age'] = driver.dob.apply(from_dob_to_age)

    # get year make and model from vehicles table, create column for accounts table
    vehicles['yearMakeModel'] = vehicles.vehicleYear.astype(str) + " " + vehicles.make + " " + vehicles.model
    vehicles['veh_age'] = datetime.date.today().year - vehicles.vehicleYear
    vehicles.VIN = vehicles.VIN.str.upper()

    oldest_driver = driver.groupby('policyID').agg(oldest_driver_age=('age', 'max')).reset_index()

    num_drivers = driver.groupby('policyID').agg(number_of_drivers=('driverID', 'count')).reset_index()
    num_vehicles = vehicles.groupby('policyID').agg(number_of_vehicles=('vehicleID', 'count')).reset_index()
    num_sr22 = driver.groupby('policyID').agg(requiresSR22=('requiresSR22', 'sum')).reset_index()

    VINS = vehicles.groupby('policyID').VIN.apply(list).reset_index()
    veh_years = vehicles.groupby('policyID').veh_age.apply(list).reset_index()
    usages = vehicles.groupby('policyID').driverUsage.apply(list).reset_index()
    ymm = vehicles.groupby('policyID').yearMakeModel.apply(list).reset_index()

    # other drivers
    other_drivers = driver[driver.driverNumber != 1]
    other_drivers['full_name'] = other_drivers.fname + ' ' + other_drivers.lname
    other_names = other_drivers.groupby('policyID').full_name.apply(list).reset_index()
    other_ages = other_drivers.groupby('policyID').age.apply(list).reset_index()
    other_genders = other_drivers.groupby('policyID').gender.apply(list).reset_index()
    other_martial_status = other_drivers.groupby('policyID').maritalStatus.apply(list).reset_index()
    other_names = other_names.rename(columns=({'full_name': 'other_names'}))
    other_ages = other_ages.rename(columns=({'age': 'other_ages'}))
    other_genders = other_genders.rename(columns=({'gender': 'other_genders'}))
    other_martial_status = other_martial_status.rename(columns=({'maritalStatus': 'other_maritalStatus'}))

    # sum of OEM and AEB
    num_OEM = vehicles.groupby('policyID').agg(OEM=('OEM', 'sum')).reset_index()
    num_AEB = vehicles.groupby('policyID').agg(AEB=('aeb', 'sum')).reset_index()

    # primary policy holder info
    primary_policy_holder = driver[driver.driverNumber.isin([1])]
    primary_policy_holder = primary_policy_holder[
        ['policyID', 'fname', 'lname', 'age', 'gender', 'maritalStatus', 'licNum', "licState", 'dateLicensed',
         'ubiScoreTier']]

    accounts = policy[
        ['insuredID', 'policyID', 'email', 'phone2', 'policyNum', 'status', 'planName', 'effectiveDate', 'insuredAddDate',
         'applicationDate', 'expirationDate', \
         'cancelledDate', 'street', 'street2', 'city', 'state', 'zipcode', 'county', 'priorCarrier',
         'priorYearsWithCompany', \
         'priorMonthsWithCompany', 'priorPolicyExpDate', 'eDelivery', 'premiumTotal', 'last_loop_exp_date', 'isRenewal',
         'total_quotes', 'fTouchDate', 'lTouchDate']]

    accounts = accounts.merge(primary_policy_holder, how='left', on='policyID')
    accounts = accounts.merge(num_drivers, how='left')
    accounts = accounts.merge(oldest_driver, how='left')
    accounts = accounts.merge(num_vehicles, how='left')
    accounts = accounts.merge(VINS, how='left')
    accounts = accounts.merge(veh_years, how='left')
    accounts = accounts.merge(ymm, how='left')

    accounts = accounts.merge(num_sr22, how='left')
    accounts = accounts.merge(num_OEM, how='left')
    accounts = accounts.merge(num_AEB, how='left')
    accounts = accounts.merge(usages, how='left')
    accounts = accounts.merge(other_names, how='left')
    accounts = accounts.merge(other_ages, how='left')
    accounts = accounts.merge(other_genders, how='left')
    accounts = accounts.merge(other_martial_status, how='left')

    # change variables to strings
    accounts.VIN = accounts.VIN.astype(str)
    accounts.veh_age = accounts.veh_age.astype(str)
    accounts.driverUsage = accounts.driverUsage.astype(str)
    accounts.yearMakeModel = accounts.yearMakeModel.astype(str)

    # remove brackets
    accounts.VIN = accounts.VIN.str.replace("[", '').str.replace("]", '')
    accounts.veh_age = accounts.veh_age.str.replace("[", '').str.replace("]", '')
    accounts.driverUsage = accounts.driverUsage.str.replace("[", '').str.replace("]", '')
    accounts.yearMakeModel = accounts.yearMakeModel.str.replace("[", '').str.replace("]", '')

    # if null, change back to null
    accounts.loc[accounts.VIN == "nan", 'VIN'] = np.nan
    accounts.loc[accounts.veh_age == "nan", 'veh_age'] = np.nan
    accounts.loc[accounts.driverUsage == "nan", 'driverUsage'] = np.nan
    accounts.loc[accounts.yearMakeModel == "nan", 'yearMakeModel'] = np.nan

    # change variables to strings
    accounts.other_names = accounts.other_names.astype(str)
    accounts.other_ages = accounts.other_ages.astype(str)
    accounts.other_genders = accounts.other_genders.astype(str)
    accounts.other_maritalStatus = accounts.other_maritalStatus.astype(str)

    # remove brackets
    accounts.other_names = accounts.other_names.str.replace("[", '').str.replace("]", '')
    accounts.other_ages = accounts.other_ages.str.replace("[", '').str.replace("]", '')
    accounts.other_genders = accounts.other_genders.str.replace("[", '').str.replace("]", '')
    accounts.other_maritalStatus = accounts.other_maritalStatus.str.replace("[", '').str.replace("]", '')

    # if null, keep null
    accounts.loc[accounts.other_names == "nan", 'other_names'] = np.nan
    accounts.loc[accounts.other_ages == "nan", 'other_ages'] = np.nan
    accounts.loc[accounts.other_genders == "nan", 'other_genders'] = np.nan
    accounts.loc[accounts.other_maritalStatus == "nan", 'other_maritalStatus'] = np.nan

    # make dates actual date times
    accounts.applicationDate = pd.to_datetime(accounts.applicationDate)
    accounts.priorPolicyExpDate = pd.to_datetime(accounts.priorPolicyExpDate)
    accounts.insuredAddDate = pd.to_datetime(accounts.insuredAddDate)
    accounts.dateLicensed = pd.to_datetime(accounts.dateLicensed)
    driver_accident.accidentDate = pd.to_datetime(driver_accident.accidentDate)

    # 2 columned coverages, these are coverages that use limit1 and limit2 fields, pivot them seperately and combine them in a temp table
    coverages = coverages.rename(columns=({'objectID': 'vehicleID'}))
    two_columns = ['BI', 'UIMBI', 'RENT']
    column_1 = coverages[coverages.coverage.isin(two_columns)].groupby(['vehicleID', 'coverage']).agg(
        limit1=('limit1', 'first')).reset_index()
    column_2 = coverages[coverages.coverage.isin(two_columns)].groupby(['vehicleID', 'coverage']).agg(
        limit2=('limit2', 'first')).reset_index()
    # pivot these tables on the deductibles column
    d = ['COLL', 'OTC']
    coverage_ded = coverages[coverages.coverage.isin(d)].groupby(['vehicleID', 'coverage']).agg(
        deductible=('deductible', 'first')).reset_index()
    # pivot everything else on the limit1 only
    other_columns = coverages[~coverages.coverage.isin(two_columns + d)].groupby(['vehicleID', 'coverage']).agg(
        limit1=('limit1', 'first')).reset_index()
    # create pivot tables for 2 columned limits
    c_1 = column_1.pivot('vehicleID', columns=['coverage'], values='limit1').reset_index()
    c_2 = column_2.pivot('vehicleID', columns=['coverage'], values='limit2').reset_index()
    # pivot table limit1 and deductible
    c_3 = other_columns.pivot('vehicleID', columns='coverage', values='limit1').reset_index()
    ded = coverage_ded.pivot('vehicleID', columns='coverage', values='deductible').reset_index()
    # rename columns for better understanding
    c_1 = c_1.rename(columns=({'BI': 'BI1', 'RENT': 'RENT1', 'UIMBI': 'UIMBI1'}))
    c_2 = c_2.rename(columns=({'BI': 'BI2', 'RENT': 'RENT2', 'UIMBI': 'UIMBI2'}))
    # merge into temp table
    temp = vehicles[['policyID', 'vehicleID']]
    temp = temp.merge(c_1, how='left').merge(c_2, how='left').merge(c_3, how='left').merge(ded, how='left')
    # consolidate 2 columed variables into 1 column
    temp['BI'] = np.nan
    temp['UIMBI'] = np.nan
    temp['RENT'] = np.nan

    for i in range(len(temp)):
        temp.BI[i] = f'{temp.BI1[i]}/{temp.BI2[i]}'
        temp.UIMBI[i] = f'{temp.UIMBI1[i]}/{temp.UIMBI2[i]}'
        temp.RENT[i] = f'{temp.RENT1[i]}/{temp.RENT2[i]}'
    temp = temp.drop(columns=(['BI1', 'BI2', 'RENT1', 'RENT2', 'UIMBI1', 'UIMBI2']))

    temp.loc[(temp.BI.str.contains('<NA>/<NA>')), 'BI'] = np.nan
    temp.loc[temp.RENT.str.contains('<NA>/<NA>'), 'RENT'] = np.nan
    temp.loc[temp.UIMBI.str.contains('<NA>/<NA>'), 'UIMBI'] = np.nan

    temp.loc[(temp.BI.str.contains('nan')) & (~temp.BI.isna()), 'BI'] = np.nan
    temp.loc[(temp.RENT.str.contains('nan')) & (~temp.RENT.isna()), 'RENT'] = np.nan
    temp.loc[(temp.UIMBI.str.contains('nan')) & (~temp.UIMBI.isna()), 'UIMBI'] = np.nan

    # merge to vehicles table
    vehicles = vehicles.merge(temp)

    # raise to policy level
    bi = vehicles.groupby('policyID').BI.apply(list).reset_index()
    cequip = vehicles.groupby('policyID').CEQUIP.apply(list).reset_index()
    loan = vehicles.groupby('policyID').LOAN.apply(list).reset_index()
    medpay = vehicles.groupby('policyID').MEDPAY.apply(list).reset_index()
    prd = vehicles.groupby('policyID').PD.apply(list).reset_index()
    pip = vehicles.groupby('policyID').PIP.apply(list).reset_index()
    uimpd = vehicles.groupby('policyID').UIMPD.apply(list).reset_index()
    coll = vehicles.groupby('policyID').COLL.apply(list).reset_index()
    otc = vehicles.groupby('policyID').OTC.apply(list).reset_index()
    uimbi = vehicles.groupby('policyID').UIMBI.apply(list).reset_index()
    rent = vehicles.groupby('policyID').RENT.apply(list).reset_index()

    # merge to accounts
    accounts = accounts.merge(bi, how='left')
    accounts = accounts.merge(cequip, how='left')
    accounts = accounts.merge(loan, how='left')
    accounts = accounts.merge(medpay, how='left')
    accounts = accounts.merge(prd, how='left')
    accounts = accounts.merge(pip, how='left')
    accounts = accounts.merge(uimpd, how='left')
    accounts = accounts.merge(coll, how='left')
    accounts = accounts.merge(otc, how='left')
    accounts = accounts.merge(uimbi, how='left')
    accounts = accounts.merge(rent, how='left')

    # turn to strings
    accounts.BI = accounts.BI.astype(str)
    accounts.CEQUIP = accounts.CEQUIP.astype(str)
    accounts.LOAN = accounts.LOAN.astype(str)
    accounts.MEDPAY = accounts.MEDPAY.astype(str)
    accounts.PD = accounts.PD.astype(str)
    accounts.PIP = accounts.PIP.astype(str)
    accounts.UIMPD = accounts.UIMPD.astype(str)
    accounts.COLL = accounts.COLL.astype(str)
    accounts.OTC = accounts.OTC.astype(str)
    accounts.UIMBI = accounts.UIMBI.astype(str)
    accounts.RENT = accounts.RENT.astype(str)

    # remove brackets
    accounts.BI = accounts.BI.str.replace("[", '').str.replace("]", '')
    accounts.CEQUIP = accounts.CEQUIP.str.replace("[", '').str.replace("]", '')
    accounts.LOAN = accounts.LOAN.str.replace("[", '').str.replace("]", '')
    accounts.MEDPAY = accounts.MEDPAY.str.replace("[", '').str.replace("]", '')
    accounts.PD = accounts.PD.str.replace("[", '').str.replace("]", '')
    accounts.PIP = accounts.PIP.str.replace("[", '').str.replace("]", '')
    accounts.UIMPD = accounts.UIMPD.str.replace("[", '').str.replace("]", '')
    accounts.COLL = accounts.COLL.str.replace("[", '').str.replace("]", '')
    accounts.OTC = accounts.OTC.str.replace("[", '').str.replace("]", '')
    accounts.UIMBI = accounts.UIMBI.str.replace("[", '').str.replace("]", '')
    accounts.RENT = accounts.RENT.str.replace("[", '').str.replace("]", '')

    # if null, keep null
    accounts.loc[accounts.PD == "<NA>", 'PD'] = np.nan
    accounts.loc[accounts.MEDPAY == "<NA>", 'MEDPAY'] = np.nan
    accounts.loc[accounts.PIP == "<NA>", 'PIP'] = np.nan
    accounts.loc[accounts.RENT == "<NA>", 'RENT'] = np.nan
    accounts.loc[accounts.COLL == "<NA>", 'COLL'] = np.nan
    accounts.loc[accounts.OTC == "<NA>", 'OTC'] = np.nan
    accounts.loc[accounts.LOAN == "<NA>", 'LOAN'] = np.nan
    accounts.loc[accounts.UIMPD == "<NA>", 'UIMPD'] = np.nan
    accounts.loc[accounts.UIMBI == "<NA>", 'UIMBI'] = np.nan
    accounts.loc[accounts.BI == "<NA>", 'BI'] = np.nan

    accounts.loc[(accounts.PD == "nan") & (~accounts.PD.isna()), 'PD'] = np.nan
    accounts.loc[(accounts.MEDPAY == "nan") & (~accounts.MEDPAY.isna()), 'MEDPAY'] = np.nan
    accounts.loc[(accounts.PIP == "nan") & (~accounts.PIP.isna()), 'PIP'] = np.nan
    accounts.loc[(accounts.RENT == "nan") & (~accounts.RENT.isna()), 'RENT'] = np.nan
    accounts.loc[(accounts.COLL == "nan") & (~accounts.COLL.isna()), 'COLL'] = np.nan
    accounts.loc[(accounts.OTC == "nan") & (~accounts.OTC.isna()), 'OTC'] = np.nan
    accounts.loc[(accounts.LOAN == "nan") & (~accounts.LOAN.isna()), 'LOAN'] = np.nan
    accounts.loc[(accounts.UIMPD == "nan") & (~accounts.UIMPD.isna()), 'UIMPD'] = np.nan
    accounts.loc[(accounts.UIMBI == "nan") & (~accounts.UIMBI.isna()), 'UIMBI'] = np.nan
    accounts.loc[(accounts.BI == "nan") & (~accounts.BI.isna()), 'BI'] = np.nan

    accounts['updated'] = pd.Timestamp('today', tz='US/Central').strftime("%m/%d/%Y")

    # cant find Roadside Assistance
    accounts['RSA'] = np.nan

    # _____________________________________________________CALCULATED FIELDS FOR ACCOUNTS TABLE_____________________________________________________________________________________

    # Average points charged per driver on policy  - number of total_points_charged / number of drivers on policy
    num_points_charged = driver_accident.groupby('policyID').agg(num_points_charged=('pointsCharged', 'sum')).reset_index()
    accounts = accounts.merge(num_points_charged, how='left')
    accounts.num_points_charged = accounts.num_points_charged.fillna(0)
    accounts['AVG_DHRF'] = (accounts.num_points_charged / accounts.number_of_drivers).astype(float)

    # number of comp collisions exceeding 1000 dollars
    comp_collision = driver_accident[(driver_accident.sdipID == 18) & (driver_accident.accidentAmount > 1000)] \
        .groupby('policyID').agg(comp_1000=('driverAccidentID', 'count')).reset_index()

    # number of not at fault accidents
    naf_count = driver_accident[(driver_accident.sdipID == 46)] \
        .groupby('policyID').agg(naf_count=('driverAccidentID', 'count')).reset_index()

    accounts = accounts.merge(comp_collision, how='left').merge(naf_count, how='left')

    # fill na with 0, no accidents or collisions on file to aggregate
    accounts.comp_1000 = accounts.comp_1000.fillna(0)
    accounts.naf_count = accounts.naf_count.fillna(0)

    # establish variable
    accounts['days_no_insurance'] = np.nan

    # utilize days with no insurance from date of expiration from prior policy and date of becoming customer
    accounts.loc[accounts.isRenewal == 0, 'days_no_insurance'] = accounts[accounts.isRenewal == 0].apply(
        lambda x: (x.applicationDate - x.priorPolicyExpDate).days, axis=1)

    # utilize exipiration of last days since last loop policy expiration and the start of new policy
    accounts.loc[accounts.isRenewal == 1, 'days_no_insurance'] = accounts[accounts.isRenewal == 1].apply(
        lambda x: (x.effectiveDate - x.last_loop_exp_date).days, axis=1)

    # if they applied for loop before their previous experition date expires (days_no_insurance is negative), then the lapse between coverage would have been 0
    accounts.loc[accounts.days_no_insurance < 0, 'days_no_insurance'] = 0
    # assign codes (0/A, 1-30/B, 31+/C, unknown /N)
    accounts.loc[accounts.days_no_insurance <= 0, 'prior_insurance_category'] = 'A'
    accounts.loc[(accounts.days_no_insurance > 0) & (accounts.days_no_insurance <= 30), 'prior_insurance_category'] = 'B'
    accounts.loc[accounts.days_no_insurance >= 31, 'prior_insurance_category'] = 'C'
    accounts.loc[accounts.days_no_insurance.isna(), 'prior_insurance_category'] = 'N'

    # grab last at fault accident date
    last_aaf = driver_accident[driver_accident.sdipID == 10] \
        .groupby('policyID').agg(last_at_fault_accident=('accidentDate', 'max')).reset_index()
    # merge to complate table
    accounts = accounts.merge(last_aaf, how='left')

    # how many months have past since the day the policy was generated and the day of the last accident
    accounts['months_since_last_aaf'] = accounts.apply(lambda x: (x.insuredAddDate - x.last_at_fault_accident).days / 30,
                                                       axis=1)

    # if the last aaf occurs 23 months or later, then accident free for 2 years but not 5
    accounts.loc[accounts.months_since_last_aaf > 23, 'accident_free_2'] = 1
    accounts.loc[accounts.months_since_last_aaf > 23, 'accident_free_5'] = 0

    # if null, no at fault accidents on record, set accident free 2 to 0 and  accident free 5 to 1, (only one applied at a time, so 5 year discount > 2 year discount)
    accounts.loc[(accounts.months_since_last_aaf.isna()) | (accounts.months_since_last_aaf > 59), 'accident_free_2'] = 0
    accounts.loc[(accounts.months_since_last_aaf.isna()) | (accounts.months_since_last_aaf > 59), 'accident_free_5'] = 1

    # if the last aaf occurs 23 months or sooner, then not accident free for 2 years or 5
    accounts.loc[accounts.months_since_last_aaf <= 23, 'accident_free_2'] = 0
    accounts.loc[accounts.months_since_last_aaf <= 23, 'accident_free_5'] = 0

    # check to see if no one is over the age of 21, if < 21 ,  set discounts to 0
    accounts.loc[accounts.oldest_driver_age < 21, 'accident_free_2'] = 0
    accounts.loc[accounts.oldest_driver_age < 21, 'accident_free_5'] = 0

    # check to see if the policy prior_insurance_category C or N , if C or in, set discounts to 0
    accounts.loc[accounts.prior_insurance_category.isin(['C', 'N']), 'accident_free_2'] = 0
    accounts.loc[accounts.prior_insurance_category.isin(['C', 'N']), 'accident_free_5'] = 0

    ## rework city, state and county based on zipcode
    engine = SearchEngine()
    city = []
    state = []
    county = []
    for i in range(len(accounts)):
        corrected = engine.by_zipcode(accounts.zipcode[i])
        try:
            city.append(corrected.city)
            state.append(corrected.state)
            county.append(corrected.county)
        except:
            city.append(np.nan)
            state.append(np.nan)
            county.append(np.nan)
    accounts['city'] = city
    accounts['state'] = state
    accounts['county'] = county

    # condense prior years and months to just months
    accounts.priorMonthsWithCompany = accounts.priorMonthsWithCompany.fillna(0)
    accounts.priorMonthsWithCompany = (accounts.priorYearsWithCompany * 12) + accounts.priorMonthsWithCompany


    max_min_last_3 = pd.DataFrame(columns=['driverID', 'max_min_3'])
    max_aaf_last_3 = pd.DataFrame(columns=['driverID', 'max_aaf_3'])
    max_naf_last_3 = pd.DataFrame(columns=['driverID', 'max_naf_3'])
    max_speed_last_3 = pd.DataFrame(columns=['driverID', 'max_spd_3'])
    max_sus_last_3 = pd.DataFrame(columns=['driverID', 'max_sus_3'])
    max_comp_last_3 = pd.DataFrame(columns=['driverID', 'max_cmp_3'])
    max_maj_last_3 = pd.DataFrame(columns=['driverID', 'max_maj_3'])
    max_dui_last_3 = pd.DataFrame(columns=['driverID', 'max_dui_3'])

    for i in range(len(accounts.applicationDate.dt.date.unique())):
        date_ = accounts.applicationDate.dt.date.unique()[i]
        three_years_ago = accounts.applicationDate.dt.date.unique()[i] - relativedelta(years=3)
        pholders = accounts[accounts.applicationDate.dt.date == date_].policyID.unique()
        da = driver_accident[(driver_accident.accidentDate.dt.date.between(three_years_ago, date_)) & (
            driver_accident.policyID.isin(pholders))]

        max_dui = da[(da.sdipDescription.str.contains('Alcohol & Drugs'))].groupby(['driverID']).agg(
            max_dui_3=('policyID', 'count')).reset_index()
        max_maj = da[(da.sdipDescription == 'Major')].groupby(['driverID']).agg(
            max_maj_3=('policyID', 'count')).reset_index()
        max_min = da[da.sdipID.isin([23, 39, 41, 33, 34, 63])].groupby(['driverID']).agg(
            max_min_3=('policyID', 'count')).reset_index()
        max_aaf = da[da.sdipID == 10].groupby(['driverID']).agg(max_aaf_3=('policyID', 'count')).reset_index()
        max_naf = da[da.sdipID == 46].groupby(['driverID']).agg(max_naf_3=('policyID', 'count')).reset_index()
        max_cmp = da[(da.sdipID == 18) & (~da.accidentAmount.isna())].groupby(['driverID']).agg(
            max_cmp_3=('policyID', 'count')).reset_index()
        max_spd = da[da.sdipID == 58].groupby(['driverID']).agg(max_spd_3=('policyID', 'count')).reset_index()
        max_sus = da[da.sdipID == 59].groupby(['driverID']).agg(max_sus_3=('policyID', 'count')).reset_index()

        if len(max_maj) > 0:
            max_maj_last_3 = max_maj_last_3.append(max_maj)
        if len(max_aaf) > 0:
            max_aaf_last_3 = max_aaf_last_3.append(max_aaf)
        if len(max_naf) > 0:
            max_naf_last_3 = max_naf_last_3.append(max_naf)
        if len(max_min) > 0:
            max_min_last_3 = max_min_last_3.append(max_min)
        if len(max_cmp) > 0:
            max_comp_last_3 = max_comp_last_3.append(max_cmp)
        if len(max_spd) > 0:
            max_speed_last_3 = max_speed_last_3.append(max_spd)
        if len(max_sus) > 0:
            max_sus_last_3 = max_sus_last_3.append(max_sus)
        if len(max_dui) > 0:
            max_dui_last_3 = max_dui_last_3.append(max_dui)

    violations = driver_accident[['driverID', 'policyID']]
    violations = violations.drop_duplicates(subset='driverID', keep='first')
    violations = violations.merge(max_aaf_last_3, how='left').merge(max_min_last_3, how='left').merge(max_comp_last_3,
                                                                                                      how='left') \
        .merge(max_speed_last_3, how='left').merge(max_sus_last_3, how='left').merge(max_naf_last_3, how='left').merge(
        max_maj_last_3, how='left').merge(max_dui_last_3, how='left')
    violations = violations.fillna(0)

    accounts['ineligible_reason'] = ''
    eligible_states = ["TX"]

    spid_max = driver.groupby('policyID').agg(highest_point_count=('sdipPoints', 'max')).reset_index()
    accounts = accounts.merge(spid_max, how='left')

    highest_num_DaA = violations.groupby('policyID').agg(dui_sum=('max_dui_3', 'sum')).reset_index()
    accounts = accounts.merge(highest_num_DaA, how='left')

    # not eligible state
    accounts.loc[~accounts.state.isin(eligible_states), 'ineligible'] = 1
    accounts.loc[~accounts.state.isin(
        eligible_states), 'ineligible_reason'] = accounts.ineligible_reason + ' ' + 'Not eligible state.'

    # missing mandatory coverages
    accounts.loc[(accounts.BI.isna()) | (accounts.PD.isna()) | (accounts.BI.str.contains('nan')) | (
        accounts.PD.str.contains('nan')), 'ineligible'] = 1
    accounts.loc[(accounts.BI.isna()) | (accounts.PD.isna()) | (accounts.BI.str.contains('nan')) | (
        accounts.PD.str.contains(
            'nan')), 'ineligible_reason'] = accounts.ineligible_reason + ' ' + 'Mandatory coverages missing from policy.'

    # driver has too many points on liscence
    accounts.loc[accounts.highest_point_count > 6, 'ineligible'] = 1
    accounts.loc[
        accounts.highest_point_count > 6, 'ineligible_reason'] = accounts.ineligible_reason + ' ' + 'Driver with too many points on license.'

    # too many alchohal related incidents on policy
    accounts.loc[accounts.dui_sum > 1, 'ineligible'] = 1
    accounts.loc[
        accounts.dui_sum > 1, 'ineligible_reason'] = accounts.ineligible_reason + ' ' + 'Policy with too many Alcohol/Drug related violations.'

    # policy has too many vehicles to insure
    accounts.loc[accounts.number_of_vehicles > 5, 'ineligible'] = 1
    accounts.loc[
        accounts.number_of_vehicles > 5, 'ineligible_reason'] = accounts.ineligible_reason + ' ' + 'Too many vehicles on policy.'

    ##driver on policy driving under suspension
    violations['ineligible_reason'] = ''
    violations.loc[violations.max_sus_3 > 0, 'ineligible'] = 1
    violations.loc[
        violations.max_sus_3 > 0, 'ineligible_reason'] = violations.ineligible_reason + " " + 'Driver on policy Driving Under Suspension.'

    # driver with more than one Alchohol/Drug violation
    violations.loc[violations.max_dui_3 > 1, 'ineligible'] = 1
    violations.loc[
        violations.max_dui_3 > 1, 'ineligible_reason'] = accounts.ineligible_reason + ' ' + 'Driver with too many Alcohol/Drug related violations.'

    # driver with combination of 1 or more aaf  and more than 1 speed or  more than  2 minor violations
    violations.loc[
        (violations.max_aaf_3 >= 1) & ((violations.max_spd_3 > 1) | (violations.max_min_3 > 2)), 'ineligible'] = 1
    violations.loc[(violations.max_aaf_3 >= 1) & ((violations.max_spd_3 > 1) | (
                violations.max_min_3 > 2)), 'ineligible_reason'] = violations.ineligible_reason + " " + "Driver with combination of AAF and Speeding/Minor violations."

    # driver with combination of more than 3 speeding and minor violations
    violations.loc[violations.max_spd_3 + violations.max_min_3 >= 3, 'ineligible'] = 1
    violations.loc[
        violations.max_spd_3 + violations.max_min_3 >= 3, 'ineligible_reason'] = violations.ineligible_reason + " " + "Driver with combination of too many speeding and minor violations."

    # driver with more than 4 comp collision violations over $1000
    violations.loc[violations.max_cmp_3 > 4, 'ineligible'] = 1
    violations.loc[
        violations.max_cmp_3 > 4, 'ineligible_reason'] = violations.ineligible_reason + " " + "Driver with too many comp violations exceeding $1000."

    # driver with more than 2 At fault Accidents (2 can be reviewed by audit)
    violations.loc[violations.max_aaf_3 > 2, 'ineligible'] = 1
    violations.loc[
        violations.max_aaf_3 > 2, 'ineligible_reason'] = violations.ineligible_reason + " " + 'Driver with More than two at fault accident violations.'

    # driver with more than 3 accidents, at fault or not.
    violations.loc[(violations.max_aaf_3 + violations.max_naf_3) > 3, 'ineligible'] = 1
    violations.loc[(
                               violations.max_aaf_3 + violations.max_naf_3) > 3, 'ineligible_reason'] = violations.ineligible_reason + ' ' + "Driver with too many Accident violations."

    # driver where at fault accidents and minor violations exceed three
    violations.loc[violations.max_aaf_3 + violations.max_min_3 > 3, 'ineligible'] = 1
    violations.loc[
        violations.max_aaf_3 + violations.max_min_3 > 3, 'ineligible_reason'] = violations.ineligible_reason + " " + "Driver with Combination of too many AAF and minor violations."

    # driver with more than 1 major violation
    violations.loc[violations.max_maj_3 > 1, 'ineligible'] = 1
    violations.loc[
        violations.max_maj_3 > 1, 'ineligible_reason'] = violations.ineligible_reason + " " + "Driver with more than 1 Major Violation"

    # driver with combo of alchohol/drug + more than 1 speed violation or more than 2 minor violations
    violations.loc[(violations.max_dui_3 > 1) & ((violations.max_spd_3 > 1) | (violations.max_min_3 > 2)), 'ineligible'] = 1
    violations.loc[(violations.max_dui_3 > 1) & ((violations.max_spd_3 > 1) | (
                violations.max_min_3 > 2)), 'ineligible_reason'] = violations.ineligible_reason + " " + "Driver with combination of Alcohol/Drug and Speeding/Minor Violations"

    violations_to_policy = violations[violations.ineligible == 1][['policyID', 'ineligible', 'ineligible_reason']]

    for i in range(len(violations_to_policy)):
        accounts.loc[accounts.policyID == violations_to_policy.policyID.values[i], 'ineligible'] = 1
        accounts.loc[accounts.policyID == violations_to_policy.policyID.values[
            i], 'ineligible_reason'] = accounts.ineligible_reason + " " + violations_to_policy.ineligible_reason.values[i]
    accounts.ineligible = accounts.ineligible.fillna(0)

    q = '''SELECT * FROM `datalakewarehouse.d2a_datasets.processed_vins`'''
    processed_vins = gbq.read_gbq(q)

    post_vehicles = vehicles

    vehicles = vehicles.merge(processed_vins, how='left', left_on='VIN', right_on='apiVIN')
    new_vins = vehicles[vehicles.apiVIN.isna()][['VIN']]
    new_vins['apiYear'] = ''
    new_vins['apiMake'] = ''
    new_vins['apiModel'] = ''
    new_vins['apiBodyType'] = ''
    new_vins['apiTrim'] = ''
    new_vins['apiEngineType'] = ''

    if len(new_vins) > 0:
        for i in range(len(new_vins)):
            try:
                current_vehicle = VIN(vehicles.VIN.values[i])
            except:
                current_vehicle = []
            if current_vehicle == []:
                new_vins.apiYear.values[i] = np.nan
                new_vins.apiMake.values[i] = np.nan
                new_vins.apiModel.values[i] = np.nan
                new_vins.apiTrim.values[i] = np.nan
                new_vins.apiBodyType.values[i] = np.nan
            else:
                try:
                    new_vins.apiYear.values[i] = current_vehicle.ModelYear
                except:
                    new_vins.apiYear.values[i] = np.nan
                try:
                    new_vins.apiMake.values[i] = current_vehicle.Make
                except:
                    new_vins.apiMake.values[i] = np.nan
                try:
                    new_vins.apiModel.values[i] = current_vehicle.Model
                except:
                    new_vins.apiModel.values[i] = np.nan
                try:
                    new_vins.apiTrim.values[i] = current_vehicle.Trim
                except:
                    new_vins.apiTrim.values[i] = np.nan
                try:
                    new_vins.apiBodyType.values[i] = current_vehicle.VehicleType
                except:
                    new_vins.apiBodyType.values[i] = np.nan
                try:
                    new_vins.apiEngineType.values[i] = current_vehicle.FuelTypePrimary
                except:
                    new_vins.apiEngineType.values[i] = np.nan

        new_vins = new_vins.rename(columns=({"VIN": "apiVIN"}))
        processed_vins = processed_vins.append(new_vins)
        processed_vins = processed_vins.drop_duplicates(subset='apiVIN')

        processed_vins = processed_vins.replace(r'^\s*$', np.nan, regex=True)
        table_id = 'datalakewarehouse.d2a_datasets.processed_vins'
        client.delete_table(table_id, not_found_ok=True)
        processed_vins.apiVIN = processed_vins.apiVIN.astype(str)
        processed_vins.apiYear = processed_vins.apiYear.astype(str)
        processed_vins.loc[processed_vins.apiYear == 'nan', 'apiYear'] = np.nan
        processed_vins.apiYear = pd.to_numeric(processed_vins.apiYear, errors='coerce')
        processed_vins.to_gbq(table_id)

        processed_vins = gbq.read_gbq('''SELECT * FROM `datalakewarehouse.d2a_datasets.processed_vins`''')
        vehicles = post_vehicles.merge(processed_vins, how='left', left_on='VIN', right_on='apiVIN')

    EngineType = vehicles.groupby('policyID').apiEngineType.apply(list).reset_index()
    accounts = accounts.merge(EngineType, how='left', on='policyID')
    accounts.apiEngineType = accounts.apiEngineType.astype(str)
    accounts.apiEngineType = accounts.apiEngineType.str.replace("[", '').str.replace("]", '')
    accounts.loc[accounts.apiEngineType == "<NA>", 'apiEngineType'] = np.nan
    accounts.loc[(accounts.apiEngineType == "nan") & (~accounts.apiEngineType.isna()), 'apiEngineType'] = np.nan

    # vehicles found by VIN API
    vehicles.loc[vehicles.apiBodyType == 'MOTORCYCLE', 'vehicle_ineligibity'] = 1
    vehicles.loc[vehicles.apiMake == 'AVANTI', 'vehicle_ineligibity'] = 1
    vehicles.loc[vehicles.apiMake == 'MCLAREN', 'vehicle_ineligibity'] = 1
    vehicles.loc[vehicles.apiMake == 'ASTON MARTIN', 'vehicle_ineligibity'] = 1
    vehicles.loc[vehicles.apiMake == 'BENTLEY', 'vehicle_ineligibity'] = 1
    vehicles.loc[(vehicles.apiMake == 'LAMBORGHINI'), 'vehicle_ineligibity'] = 1
    vehicles.loc[(vehicles.apiMake == 'MASERATI'), 'vehicle_ineligibity'] = 1
    vehicles.loc[(vehicles.apiMake == 'MCLAREN'), 'vehicle_ineligibity'] = 1
    vehicles.loc[vehicles.apiMake == 'FERRARI', 'vehicle_ineligibity'] = 1
    vehicles.loc[(vehicles.apiMake == 'ROLLS ROYCE'), 'vehicle_ineligibity'] = 1
    vehicles.loc[(vehicles.apiMake == 'FORD') & (vehicles.apiModel == "GT"), 'vehicle_ineligibity'] = 1
    vehicles.loc[(vehicles.apiMake == 'HUMMER') & (vehicles.apiModel == "H1"), 'vehicle_ineligibity'] = 1
    # vehicles not found by VIN API
    vehicles.loc[(vehicles.make.str.upper().str.contains("EXCALIBUR")) & (~vehicles.make.isna()), 'vehicle_ineligibity'] = 1
    vehicles.loc[(vehicles.make.str.upper().str.contains("SHELBY")) & (~vehicles.make.isna()), 'vehicle_ineligibity'] = 1
    vehicles.loc[(vehicles.make == 'JENSEN') & (vehicles.model == "INTERCEPTOR "), 'vehicle_ineligibity'] = 1
    vehicles.loc[vehicles.make == 'DELOREAN', 'vehicle_ineligibity'] = 1
    vehicles.loc[vehicles.model == 'PANTERA', 'vehicle_ineligibity'] = 1
    vehicles.loc[(vehicles.make == 'VECTOR'), 'vehicle_ineligibity'] = 1

    vehicles.loc[vehicles.vehicle_ineligibity == 1, 'vehicle_ineligibity_reason'] = 'Non-Insurable Vehicle on Policy.'
    inel_vehicles = vehicles[vehicles.vehicle_ineligibity == 1][
        ['policyID', 'vehicle_ineligibity', 'vehicle_ineligibity_reason']]
    accounts = accounts.merge(inel_vehicles, how='left', on='policyID')

    accounts.loc[accounts.vehicle_ineligibity == 1, 'ineligible'] = 1
    accounts.loc[
        accounts.vehicle_ineligibity == 1, 'ineligible_reason'] = accounts.ineligible_reason + ' ' + accounts.vehicle_ineligibity_reason
    accounts = accounts.drop(columns=['vehicle_ineligibity', 'vehicle_ineligibity_reason'])

    accounts.loc[accounts.ineligible_reason == '', 'ineligible_reason'] = np.nan

    # create accounts table after deduplication process complete
    accounts = accounts.rename(columns=({'total_quotes': 'number_of_touches'}))
    accounts = accounts.rename(columns=({'apiEngineType': 'engineType'}))

    accounts = accounts[
        ['insuredID', 'policyID', 'policyNum', 'status', 'applicationDate', 'insuredAddDate', 'effectiveDate',
         'cancelledDate', 'number_of_touches' \
            , 'fTouchDate', 'lTouchDate', 'email', 'phone2', 'fname', 'lname', 'age', 'gender', 'maritalStatus',
         'other_names', 'other_ages', 'other_genders', 'other_maritalStatus' \
            , 'number_of_vehicles', 'number_of_drivers', 'num_points_charged', 'AVG_DHRF', 'ubiScoreTier' \
            , 'street', 'street2', 'city', 'state', 'zipcode', 'county', 'VIN', 'veh_age', 'yearMakeModel', 'driverUsage',
         'engineType' \
            , 'BI', 'PD', 'MEDPAY', 'PIP', 'RENT', 'COLL', 'OTC', 'UIMBI', 'UIMPD', 'LOAN' \
            , 'planName', 'eDelivery', 'requiresSR22', 'OEM', 'AEB', 'RSA', 'months_since_last_aaf', 'accident_free_2',
         'accident_free_5', 'naf_count', 'comp_1000' \
            , 'prior_insurance_category', 'priorCarrier', 'priorMonthsWithCompany', 'priorPolicyExpDate', 'premiumTotal',
         'ineligible', 'ineligible_reason', 'updated']]

    # add accounts table to undeduplicated dataset
    accounts_undeduplicated = accounts
    table_id = 'datalakewarehouse.d2a_datasets.accounts'
    client.delete_table(table_id, not_found_ok=True)
    accounts.to_gbq(table_id)
    # ---------------------------------------------------------------------------------------DEDUPLICATION PROCESS -------------------------------------------------------------

    # this will remove all duplicates based on status
    for status in policy.status.unique():
        policy[policy.status == status] = policy[policy.status == status].drop_duplicates(subset=['email'], keep='first')

    # drop null policyID
    policy = policy.dropna(subset=['policyID']).reset_index(drop=True)

    # hierarchy of importance
    # Bound                                Not Bound
    # Active -> Bound -> Canceled |||- > Expired -> Not_Written  -> RC2 -> RC1B -> RC1A

    # filter the dataframe to remove any previous quote attempts from bound policies
    status_to_skip = ['Active']
    bound_emails = policy[policy.status.isin(status_to_skip)].email.unique()
    index_to_drop = []
    for i in range(len(policy)):
        if ((policy.status[i] not in status_to_skip) and (policy.email[i] in bound_emails)):
            index_to_drop.append(i)
    policy = policy.drop(index=index_to_drop)
    policy.reset_index(inplace=True, drop=True)

    # Bound
    status_to_skip.append('Bound')
    bound_emails = policy[policy.status == 'Bound'].email.unique()
    index_to_drop = []
    for i in range(len(policy)):
        if ((policy.status[i] not in status_to_skip) and (policy.email[i] in bound_emails)):
            index_to_drop.append(i)
    policy = policy.drop(index=index_to_drop)
    policy.reset_index(inplace=True, drop=True)

    # Canceled
    status_to_skip.append('Canceled')
    Canceled_emails = policy[policy.status == 'Canceled'].email.unique()
    index_to_drop = []
    for i in range(len(policy)):
        if ((policy.status[i] not in status_to_skip) and (policy.email[i] in Canceled_emails)):
            index_to_drop.append(i)
    policy = policy.drop(index=index_to_drop)
    policy.reset_index(inplace=True, drop=True)

    # Expired
    status_to_skip.append('Expired')
    expired_emails = policy[policy.status == 'Expired'].email.unique()
    index_to_drop = []
    for i in range(len(policy)):
        if ((policy.status[i] not in status_to_skip) and (policy.email[i] in expired_emails)):
            index_to_drop.append(i)
    policy = policy.drop(index=index_to_drop)
    policy.reset_index(inplace=True, drop=True)

    # Declined
    status_to_skip.append('Declined')
    declined = policy[policy.status == 'Declined'].email.unique()
    index_to_drop = []
    for i in range(len(policy)):
        if ((policy.status[i] not in status_to_skip) and (policy.email[i] in declined)):
            index_to_drop.append(i)
    policy = policy.drop(index=index_to_drop)
    policy.reset_index(inplace=True, drop=True)

    # Not_Written emails
    status_to_skip.append('Not_Written')
    not_written_emails = policy[policy.status == 'Not_Written'].email.unique()
    index_to_drop = []
    for i in range(len(policy)):
        if ((policy.status[i] not in status_to_skip) and (policy.email[i] in not_written_emails)):
            index_to_drop.append(i)
    policy = policy.drop(index=index_to_drop)
    policy.reset_index(inplace=True, drop=True)

    # RC2
    status_to_skip.append('RC2')
    rc2_emails = policy[policy.status == 'RC2'].email.unique()
    index_to_drop = []
    for i in range(len(policy)):
        if ((policy.status[i] not in status_to_skip) and (policy.email[i] in rc2_emails)):
            index_to_drop.append(i)
    policy = policy.drop(index=index_to_drop)
    policy.reset_index(inplace=True, drop=True)

    # RC1B
    status_to_skip.append('RC1B')
    RC1B_emails = policy[policy.status == 'RC1B'].email.unique()
    index_to_drop = []
    for i in range(len(policy)):
        if ((policy.status[i] not in status_to_skip) and (policy.email[i] in RC1B_emails)):
            index_to_drop.append(i)
    policy = policy.drop(index=index_to_drop)
    policy.reset_index(inplace=True, drop=True)

    # RC1A
    status_to_skip.append('RC1A')
    RC1A_emails = policy[policy.status == 'RC1A'].email.unique()
    index_to_drop = []
    for i in range(len(policy)):
        if ((policy.status[i] not in status_to_skip) and (policy.email[i] in RC1A_emails)):
            index_to_drop.append(i)
    policy = policy.drop(index=index_to_drop)
    policy.reset_index(inplace=True, drop=True)

    # a check to see how the amount paid should be calculated
    # if policy is still in expired status after deduplicaing the data, then the amountPaid should be the sum of the amountPaid for all expired policies(histAmountPaid)
    # if policy is in active or canceled after deduplicating data, then the amountpaid should be the sum of all old policies amountPaid + amountPaid on current Policy
    policy.loc[~policy.status.isin(['Expired']), 'histAmountPaid'] = policy[~policy.status.isin(['Expired'])].amountPaid + \
                                                                     policy[~policy.status.isin(['Expired'])].histAmountPaid

    # remove xemplar dummy policies
    policy = policy[~policy.insuredID.isin([24, 25])]

    # remove any null emails
    policy = policy[~policy.email.isna()]

    # drop any extra by policyID , keep first
    policy = policy.drop_duplicates(subset='policyID', keep='first')
    accounts = accounts.drop_duplicates(subset='policyID', keep='first')

    # deduplicate the other tables using the policyIDs that have already been deduplicated
    p_id = policy.policyID.unique()
    i_id = policy.insuredID.unique()
    a_id = insured[insured.insuredID.isin(i_id)].mailingAddressID.unique()

    # limit other tables to only include policyIDs of undeduplicated silvervine
    driver = driver[driver.policyID.isin(p_id)]
    vehicles = vehicles[vehicles.policyID.isin(p_id)]
    address = address[address.mailingAddressID.isin(a_id)]
    auto = auto[auto.policyID.isin(p_id)]
    driver_accident = driver_accident[driver_accident.policyID.isin(p_id)]
    coverages = coverages[coverages.policyID.isin(p_id)]
    insured = insured[insured.insuredID.isin(i_id)]
    accounts = accounts[accounts.policyID.isin(p_id)]

    # ----------------------------ADD Adjusted tables to Bigquery ---------------------------------------------------------------------------------#
    # create big query tables, delete the old one if exists, re-upload new table
    table_id = 'datalakewarehouse.d2a_datasets.silvervine_deduplicated_policy'
    client.delete_table(table_id, not_found_ok=True)
    policy.to_gbq(table_id)

    table_id = 'roadrisk-244718.silvervine_deduplicated.policy'
    client.delete_table(table_id, not_found_ok=True)
    policy.to_gbq(table_id)

    table_id = 'datalakewarehouse.d2a_datasets.silvervine_deduplicated_insured'
    client.delete_table(table_id, not_found_ok=True)
    insured.to_gbq(table_id)

    table_id = 'roadrisk-244718.silvervine_deduplicated.insured'
    client.delete_table(table_id, not_found_ok=True)
    insured.to_gbq(table_id)

    table_id = 'datalakewarehouse.d2a_datasets.silvervine_deduplicated_driver'
    client.delete_table(table_id, not_found_ok=True)
    driver.to_gbq(table_id)

    table_id = 'roadrisk-244718.silvervine_deduplicated.driver'
    client.delete_table(table_id, not_found_ok=True)
    driver.to_gbq(table_id)

    # territory loaded as string
    vehicles.territory = vehicles.territory.astype(str)
    table_id = 'datalakewarehouse.d2a_datasets.silvervine_deduplicated_vehicles'
    client.delete_table(table_id, not_found_ok=True)
    vehicles.to_gbq(table_id)

    table_id = 'roadrisk-244718.silvervine_deduplicated.vehicles'
    client.delete_table(table_id, not_found_ok=True)
    vehicles.to_gbq(table_id)

    table_id = 'datalakewarehouse.d2a_datasets.silvervine_deduplicated_auto'
    client.delete_table(table_id, not_found_ok=True)
    auto.to_gbq(table_id)

    table_id = 'roadrisk-244718.silvervine_deduplicated.auto'
    client.delete_table(table_id, not_found_ok=True)
    auto.to_gbq(table_id)

    table_id = 'datalakewarehouse.d2a_datasets.silvervine_deduplicated_driver_accident'
    client.delete_table(table_id, not_found_ok=True)
    driver_accident.to_gbq(table_id)

    table_id = 'roadrisk-244718.silvervine_deduplicated.driver_accident'
    client.delete_table(table_id, not_found_ok=True)
    driver_accident.to_gbq(table_id)

    table_id = 'datalakewarehouse.d2a_datasets.silvervine_deduplicated_coverages'
    client.delete_table(table_id, not_found_ok=True)
    coverages.to_gbq(table_id)

    table_id = 'roadrisk-244718.silvervine_deduplicated.coverages'
    client.delete_table(table_id, not_found_ok=True)
    coverages.to_gbq(table_id)

    table_id = 'datalakewarehouse.d2a_datasets.silvervine_deduplicated_address'
    client.delete_table(table_id, not_found_ok=True)
    address.to_gbq(table_id)

    table_id = 'roadrisk-244718.silvervine_deduplicated.address'
    client.delete_table(table_id, not_found_ok=True)
    address.to_gbq(table_id)

    table_id = 'datalakewarehouse.d2a_datasets.silvervine_deduplicated_accounts'
    client.delete_table(table_id, not_found_ok=True)
    accounts.to_gbq(table_id)

    table_id = 'roadrisk-244718.silvervine_deduplicated.accounts'
    client.delete_table(table_id, not_found_ok=True)
    accounts.to_gbq(table_id)

if __name__ == '__main__':
    create_accounts()